/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bda_maestro_detalle;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author MCROBERTW
 */
public class ConexionPostgresql {
    private Connection conexionBD;
    public Connection getConexion() {
        return conexionBD;
    }       
    public void setConexion(Connection conexionBD) {
        this.conexionBD = conexionBD;
    }
    public ConexionPostgresql conectar() {
        try {
            // carga el driver y oracle
          Class.forName("org.postgresql.Driver");
          

          //crea una variable con la direccion el puerto y la instancia (express)
         String BaseDeDatos ="jdbc:postgresql://172.30.102.28:5432/BasePrueba?autoReconnect=true&relaxAutoCommit=true";
         
         // carga la conexion (usuario contraseña)
         conexionBD = DriverManager.getConnection(BaseDeDatos, "facci","facci2019");           

         if (conexionBD != null) {
             JOptionPane.showMessageDialog(null, "Conectado a la base de datos Postresql!");
         } else {
             JOptionPane.showMessageDialog(null, "Error en la Conexión !");
         }
        } catch (Exception e) {
             JOptionPane.showMessageDialog(null, e.getMessage()+"aqui es");
        }
        return this;
    }
    
    public boolean ejecutar(String sql) { //
        try {
            Statement sentencia; // objetos para sentencias de oracle 
            sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY); 
            sentencia.executeUpdate(sql); //ejecuta el insert delete y el updte 
            
            getConexion().commit();
            
            
            
            //sentencia.getConnection().setAutoCommit(false);
        
        } catch (SQLException e) {
            if (e.getErrorCode()==0) return false; //Por error "Cannot commit when autocommit is enabled"
            JOptionPane.showMessageDialog(null, e.getErrorCode());
            return false;
        }        return true;
    }
}
